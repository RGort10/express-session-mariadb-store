export = MariaDBStore;
declare class MariaDBStore {
    constructor(options?: {});
    sessionTable: any;
    pool: any;
    get(sid: any, callback?: () => void): void;
    set(sid: any, session: any, callback?: () => void): void;
    destroy(sid: any, callback?: () => void): void;
    clear(callback?: () => void): void;
    all(callback?: () => void): void;
    length(callback?: () => void): void;
    touch(sid: any, session: any, callback?: () => void): void;
    query(query: any, data: any): any;
}
